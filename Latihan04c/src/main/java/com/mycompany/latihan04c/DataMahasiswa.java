/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan04c;
import java.util.Scanner;
/**
 *
 * @author user
 */
public class DataMahasiswa {
    Scanner scan = new Scanner(System.in);
    String nama;
    String prodi;
    Integer nilai;
    char ketNilai;
    void kNilai(){
        if (nilai <= 100 && nilai >= 85){
            ketNilai = 'A';
        } else if (nilai < 85 && nilai >= 70) {
            ketNilai = 'B';
        } else if (nilai < 70 && nilai >= 60) {
            ketNilai = 'C';
        } else if (nilai < 60 && nilai >= 50) {
            ketNilai = 'D';
        } else if (nilai < 50 && nilai >= 0){
            ketNilai = 'E';
        }
    }
    void print() {
        System.out.println("Data Test");
        System.out.println("=======================================");
        System.out.print("Nama : ");
        nama = scan.nextLine();
        System.out.print("Program Studi : ");
        prodi = scan.nextLine();
        System.out.print("Nilai : ");
        nilai = scan.nextInt();
        kNilai();
        System.out.println("Nilai Huruf : " + ketNilai);
        System.out.println("=======================================");
    }
}
