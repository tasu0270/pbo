/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.laihan04d;
import java.util.Scanner;
/**
 *
 * @author user
 */
public class Latihan04d {

    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        Pembayaran p = new Pembayaran();
        System.out.println("Perhitungan Biaya Pemakaian Air");
        System.out.println("=====================");
        System.out.print("Nama\t\t: ");
        p.nama=in.nextLine();
        System.out.print("No. Pelanggan\t: ");
        p.nopel=in.nextLine();
        System.out.print("Pemakaian Air\t: ");
        p.pakai=in.nextInt();
        p.hitung();
        System.out.println("Biaya Pakai\t: "+p.totBiaya);
        System.out.println("=====================");
    }
}
